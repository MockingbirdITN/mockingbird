/// <reference path="../../js/Herramientas.js" />

$(document).ready(function () {
    loadPage_Annon();
    $("#btnEnviarSolicitud").click(function () {

        EnviarSolicitud();
        return false;

    });
    CargarPaises();
});

function CargarPaises() {
    $("#cmbPais").select2();
    AjaxPost("../../wConsultaCombos.asmx/CargarPaises", "", false, function (lstItems) {
        CargarCombo($("#cmbPais"), lstItems);

        return false;
    });
    return false;
}

function EnviarSolicitud() {
    var objSolicitud = {
        strNombreInstitucion: $("#txtNombreInstitucion").val(),
        strContacto: $("#txtContacto").val(),
        strEmail: $("#txtCorreo").val(),
        strTelefono: $("#txtTelefono").val(),
        intPais: $("#cmbPais").val(),
        strDireccion: $("#txtDireccion").val()
    };
    AjaxPost("../wsRegistroAnonimo.asmx/EnviarSolicitudInstitucion", "'objSolicitud':" + JSON.stringify(objSolicitud), false, function (boolResult) {
        if (boolResult === true) {

            window.location = "../../Confirmacion/Confirmacion/Confirmacion_Solicitud.html";

        }
        else {
            window.location = "../../Confirmacion/Error/Error_Solicitud.html";
        }
    });
    return false;
}