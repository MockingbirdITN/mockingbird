///<reference path="../../js/Herramientas.js" />

$(document).ready(function () {
    loadPage_Annon();
    $("#btnEnviarSolicitud").click(function () {

        EnviarSolicitud();
        return false;

    });
    CargarInstituciones();
});

function CargarInstituciones() {
    $("#cmbInstitucion").select2();
    AjaxPost("../../wConsultaCombos.asmx/CargarInstituciones", "", false, function (lstItems) {
        CargarCombo($("#cmbInstitucion"), lstItems);

        return false;
    });
    return false;
}

function EnviarSolicitud() {

    var objSolicitud = {
        strNombre: $("#strNombre").val(),
        strEmail: $("#txtCorreo").val(),
        strPassword: $("#txtPassword").val(),
        intInstitucion: $("#cmbInstitucion").val(),
        strNumeroControl: $("#txtNumeroControl").val()
    };

    AjaxPost("../wsRegistroAnonimo.asmx/EnviarSolicitudUsuario", "'objSolicitud':" + JSON.stringify(objSolicitud), false, function (boolResult) {

        if (boolResult) {
            window.location = "../../Confirmacion/Confirmacion/Confirmacion_Solicitud.html";
        }
        else {
            window.location = "../../Confirmacion/Error/Error_Solicitud.html";
        }

        return false;

    });

    return false;
}