﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Configuration;

/// <summary>
/// Summary description for wConsultaCombos
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class wConsultaCombos : System.Web.Services.WebService
{

    public wConsultaCombos()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public List<clsComboItems.ComboItem> CargarPaises()
    {
        return clsPais.GetComboPaises();
    }
    [WebMethod]
    public List<clsComboItems.ComboItem> CargarGrados()
    {
        return clsGrado.GetCombo();
    }
    [WebMethod]
    public List<clsComboItems.ComboItem> CargarInstituciones()
    {
        return clsInstitucion.GetComboInstituciones();
    }
    [WebMethod]
    public List<clsComboItems.ComboItem> CargarCategorias()
    {
        return clsCategoriaPrograma.GetCombo();
    }
    [WebMethod]
    public List<clsComboItems.ComboItem> CargarSubCategorias(int intCategoriaId)
    {
        return clsSubCategoriaPrograma.GetCombo(intCategoriaId);
    }
    [WebMethod]
    public List<clsComboItems.ComboItem> CargarUsuariosVinculacion(int intInstitucionId)
    {
        return clsUsuario.GetComboTipoVinculacion(intInstitucionId);
    }
    [WebMethod]
    public List<clsComboItems.ComboItem> CargarTipoProgramas()
    {
        return clsTipoPrograma.GetCombo();
    }
}
