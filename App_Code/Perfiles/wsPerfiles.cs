﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for wsPerfiles
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class wsPerfiles : System.Web.Services.WebService
{

    public wsPerfiles()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region Programa

    [WebMethod]
    public clsPrograma.Programa GetPrograma(int intProgramaId)
    {
        return clsPrograma.getPrograma(intProgramaId);
    }

    [WebMethod]
    public clsPrograma.ProgramaPerfil GetProgramaPerfil(int intProgramaId)
    {
        return clsPrograma.GetProgramaPerfil(intProgramaId);
    }

    [WebMethod]
    public clsNotificacion InsertarPrograma(clsPrograma.Programa objPrograma)
    {
        return clsPrograma.Insertar(objPrograma);
    }

    #endregion

    #region Institucion


    [WebMethod]
    public clsInstitucion.InstitucionPerfil GetInstitucionPerfil(int intInstitucionId)
    {
        return clsInstitucion.GetInstitucionPerfil(intInstitucionId);
    }
    [WebMethod]
    public List<clsPrograma.Programa> GetListaProgramasInstitucion(int intInstitucionId)
    {
        return clsPrograma.getListaProgramasInstitucion(intInstitucionId);
    }
    [WebMethod]
    public List<clsUsuario.Usuario> GetListaUsuariosInstitucion(int intInstitucionId)
    {
        return clsUsuario.getListaUsuarios(intInstitucionId);
    }
    #endregion

    #region Usuario
    [WebMethod(EnableSession = true)]
    public clsUsuario.UsuarioPerfil CargarUsuarioPerfil()
    {
        clsUsuario.UsuarioPerfil Perfil = ((clsUsuario.Usuario)Session["Usuario"]).CargarPerfilUsuario();

        return Perfil;
    }
    public class ActualizacionUsuarioPerfil{
        public int UsuarioId;
        public string NombreUsuario;
        public string Nombre;
        public string Correo;
        public string Telefono;
        public int Pais;
        public string FechaNacimiento;
        public int GradoId;
        }

    [WebMethod(EnableSession = true)]
    public clsNotificacion ActualizarUsuarioPerfil(ActualizacionUsuarioPerfil ActualizacionUsuarioPerfil) {
        clsUsuario.Usuario usuario = new clsUsuario.Usuario();
        usuario.intUsuarioId = ActualizacionUsuarioPerfil.UsuarioId;
        usuario.strNombreUsuario = ActualizacionUsuarioPerfil.NombreUsuario;
        usuario.strNombre = ActualizacionUsuarioPerfil.Nombre;
        usuario.strCorreo = ActualizacionUsuarioPerfil.Correo;
        usuario.strTelefono = ActualizacionUsuarioPerfil.Telefono;
        usuario.intPais = ActualizacionUsuarioPerfil.Pais;
        usuario.strFechaNacimiento = ActualizacionUsuarioPerfil.FechaNacimiento;
        usuario.intGradoId = ActualizacionUsuarioPerfil.GradoId;

        return ((clsUsuario.Usuario)Session["Usuario"]).ActualizarPerfilUsuario(usuario);
    }
    #endregion
}
