﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for wsInicioSesion
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class wsInicioSesion : System.Web.Services.WebService
{

    public wsInicioSesion()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }


    public class clsInicioSesionUsuario
    {

        public string strNombreUsuario;
        public string strPassword;
    }

    [WebMethod (EnableSession = true)]
    public int IniciarSesion(clsInicioSesionUsuario objSolicitud)
    {
        clsUsuario usuario = new clsUsuario();
        usuario.strNombreUsuario = objSolicitud.strNombreUsuario;
        usuario.strPassword = objSolicitud.strPassword;

        clsUsuario.Usuario Sesion = usuario.IniciarSesion();

        if (Session.Count == 0)
        {
            
            Session.Add("Usuario", Sesion);

            return ((clsUsuario.Usuario)Session["Usuario"]).intTipoUsuario;
        }
        else
        {
            return 0;
        }
    }
    [WebMethod (EnableSession = true)]
    public int RevisarSesion()
    {
        if (Session.Count != 0)
        {
            return ((clsUsuario.Usuario)Session["Usuario"]).intTipoUsuario;
        }
        else
        {
            return 0;
        }
    }

    [WebMethod(EnableSession = true)]
    public void CerrarSesion()
    {
        Session.Clear();
    }

}
