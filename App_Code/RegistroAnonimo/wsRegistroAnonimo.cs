﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
/// <summary>
/// Summary description for wsRegistroAnonimo
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class wsRegistroAnonimo : System.Web.Services.WebService
{

    public wsRegistroAnonimo()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    public class clsSolicitudInstitucion
    {
        public string strNombreInstitucion;
        public string strContacto;
        public string strEmail;
        public string strTelefono;
        public int intPais;
        public string strDireccion;
    }



    [WebMethod]
    public bool EnviarSolicitudInstitucion(clsSolicitudInstitucion objSolicitud)
    {

        string strSql = "SELECT * FROM tConfiguracion";
        DataTable dtConfiguracion;
        dtConfiguracion = clsHerramientas.GetDataTable(strSql);



        bool boolEnvioExito = false;
        if (dtConfiguracion.Rows.Count > 0)
        {
            string strBody;
            List<string> lstMails = new List<string>();
            foreach (DataRow correo in dtConfiguracion.Rows)
            {
                lstMails.Add(correo["CorreoAdministrador"].ToString());
            }
            strBody = "La institución " + objSolicitud.strNombreInstitucion + " quiere formar parte de OST./Mockigbird se encuentra en " + objSolicitud.strDireccion + ", " + objSolicitud.intPais +
                " Contacto: " + objSolicitud.strContacto + ", email: " + objSolicitud.strEmail;

            boolEnvioExito = clsHerramientas.EnviarMail(dtConfiguracion.Rows[0]["ServidorSMTP"].ToString(), dtConfiguracion.Rows[0]["UsuarioSMTP"].ToString(), dtConfiguracion.Rows[0]["PasswordSMTP"].ToString(),
                 strBody, "Solicitud de institucion", lstMails);

            if (boolEnvioExito == true)
            {
                clsInstitucion institucion = new clsInstitucion();
                institucion.strNombreInstitucion = objSolicitud.strNombreInstitucion;
                institucion.strDireccion = objSolicitud.strDireccion;
                institucion.intPais = objSolicitud.intPais;
                institucion.strEstatus = "P";

                if (institucion.Insertar().intCodigo == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }
        else
        {

            return false;

        }

        return boolEnvioExito;
    }


    public class clsSolicitudUsuario
    {

        public string strNombre;
        public string strEmail;
        public string strPassword;
        public int intInstitucion;
        public string strNumeroControl;

    }

    [WebMethod]
    public bool EnviarSolicitudUsuario(clsSolicitudUsuario objSolicitud)
    {

        string strSql = "SELECT * FROM tConfiguracion";
        DataTable dtConfiguracion;
        dtConfiguracion = clsHerramientas.GetDataTable(strSql);

        bool boolEnvioExito = false;
        if (dtConfiguracion.Rows.Count > 0)
        {

            strSql = "";

            string strBody;
            List<string> lstMails = new List<string>();
            foreach (DataRow correo in dtConfiguracion.Rows)
            {
                lstMails.Add(correo["CorreoAdministrador"].ToString());
            }
            strBody = "El alumno " + objSolicitud.strNombre + " quiere formar parte de OST./Mockigbird";

            boolEnvioExito = clsHerramientas.EnviarMail(dtConfiguracion.Rows[0]["ServidorSMTP"].ToString(), dtConfiguracion.Rows[0]["UsuarioSMTP"].ToString(), dtConfiguracion.Rows[0]["PasswordSMTP"].ToString(),
                 strBody, "Solicitud de usuario", lstMails);

            if (boolEnvioExito == true) {
                clsUsuario usuario = new clsUsuario();
                usuario.strNombreUsuario = objSolicitud.strEmail;
                usuario.strNombre = objSolicitud.strNombre;
                usuario.strCorreo = objSolicitud.strEmail;
                usuario.strPassword = objSolicitud.strPassword;
                usuario.strEstatus = "P";
                usuario.intInstitucion = objSolicitud.intInstitucion;
                usuario.strNumeroControl = objSolicitud.strNumeroControl;
                usuario.intTipoUsuario = 1;

                if (usuario.Insertar().intCodigo == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
        return boolEnvioExito;
    }
}
