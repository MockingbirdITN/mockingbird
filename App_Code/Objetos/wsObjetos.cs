﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
/// <summary>
/// Summary description for wsObjetos
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class wsObjetos : System.Web.Services.WebService
{

    public wsObjetos()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public clsCategoriaPrograma.CategoriaPrograma GetCategoriaPorId(int Id)
    {
        return clsCategoriaPrograma.GetCategoriaPrograma(Id);
    }

    [WebMethod]
    public List<clsInstitucion.InstitucionFiltro> GetInstitucionesFiltro(bool bolTop)
    {
        return clsInstitucion.GetInstitucionesFiltro(bolTop);
    }

    [WebMethod]
    public List<clsSubCategoriaPrograma.AuxIndexCategoriaSubCategoria> CargarCategoriasIndex()
    {
        return clsSubCategoriaPrograma.CargarCategoriasIndex();
    }
    [WebMethod]
    public List<clsPrograma.Programa> GetUltimosProgramas()
    {
        return clsPrograma.GetUltimosProgramas();
    }
}
