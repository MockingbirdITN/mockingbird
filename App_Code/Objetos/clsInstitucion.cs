﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for clsPrograma
/// </summary>
public class clsInstitucion
{
    public int intInstitucionId, intPais, intPublica;
    public string strNombreInstitucion,
        strLema,
        strLatitud,
        strLongitud,
        strAcercaDe,
        strDireccion,
        strTelefono,
        strCorreo,
        strLinkFacebook,
        strLinkTwitter,
        strWeb,
        strLogo,
        strFechaVencimiento,
        strUsuarioAlta,
        strEstatus;

    public clsInstitucion()
    {

    }
    //INSERT INSTITUCION
    public class Institucion
    {
        public int intInstitucionId, intPais, intPublica;
        public string strNombreInstitucion,
            strLema,
            strLatitud,
            strLongitud,
            strAcercaDe,
            strDireccion,
            strTelefono,
            strCorreo,
            strLinkFacebook,
            strLinkTwitter,
            strWeb,
            strLogo,
            strFechaVencimiento,
            strUsuarioAlta,
            strEstatus;
    }
    public clsInstitucion(Institucion objInstitucion)
    {
        intInstitucionId = objInstitucion.intInstitucionId;
        strNombreInstitucion = objInstitucion.strNombreInstitucion;
        strLema = objInstitucion.strLema;
        strLatitud = objInstitucion.strLatitud;
        strLongitud = objInstitucion.strLongitud;
        strAcercaDe = objInstitucion.strAcercaDe;
        intPais = objInstitucion.intPais;
        strDireccion = objInstitucion.strDireccion;
        strTelefono = objInstitucion.strTelefono;
        strCorreo = objInstitucion.strCorreo;
        intPublica = objInstitucion.intPublica;
        strLinkFacebook = objInstitucion.strLinkFacebook;
        strLinkTwitter = objInstitucion.strLinkTwitter;
        strWeb = objInstitucion.strWeb;
        strLogo = objInstitucion.strLogo;
        strFechaVencimiento = objInstitucion.strFechaVencimiento;
        strUsuarioAlta = objInstitucion.strUsuarioAlta;
        strEstatus = objInstitucion.strEstatus;
    }

    //PERFIL INSTITUCION
    public class InstitucionPerfil
    {

        public string InstitucionId, NombreInstitucion, Lema, Latitud, Longitud,
            AcercaDe, Pais, Direccion, Telefono, Correo, Publica, LinkFacebook, LinkTwitter, Web, Logo, FechaVencimiento,
            UsuarioAlta, FechaAlta, UsuarioModifico, FechaModifico, Estatus;

    }

    public static InstitucionPerfil GetInstitucionPerfil(int intInstitucionId)
    {
        string strSql = "SELECT * FROM tInstituciones  WHERE InstitucionId = " + intInstitucionId.ToString();
        //"vwInstitucionPerfil WHERE InstitucionId = " + intInstitucionId.ToString();
        DataTable tPerfil = clsHerramientas.GetDataTable(strSql);

        InstitucionPerfil objPerfil = new InstitucionPerfil();

        if (tPerfil.Rows.Count > 0)
        {
            //Perfil
            objPerfil.InstitucionId = tPerfil.Rows[0]["InstitucionId"].ToString();
            objPerfil.NombreInstitucion = tPerfil.Rows[0]["NombreInstitucion"].ToString();
            objPerfil.Lema = tPerfil.Rows[0]["Lema"].ToString();
            objPerfil.Latitud = tPerfil.Rows[0]["Latitud"].ToString();
            objPerfil.Longitud = tPerfil.Rows[0]["Longitud"].ToString();
            objPerfil.AcercaDe = tPerfil.Rows[0]["AcercaDe"].ToString();
            objPerfil.Pais = tPerfil.Rows[0]["Pais"].ToString();
            objPerfil.Direccion = tPerfil.Rows[0]["Direccion"].ToString();
            objPerfil.Telefono = tPerfil.Rows[0]["Telefono"].ToString();
            objPerfil.Correo = tPerfil.Rows[0]["Correo"].ToString();
            objPerfil.Publica = tPerfil.Rows[0]["Publica"].ToString();
            objPerfil.LinkFacebook = tPerfil.Rows[0]["LinkFacebook"].ToString();
            objPerfil.LinkTwitter = tPerfil.Rows[0]["LinkTwitter"].ToString();
            objPerfil.Web = tPerfil.Rows[0]["Web"].ToString();
            //objPerfil.Logo = tPerfil.Rows[0]["Logo"].ToString();

        }

        return objPerfil;

    }

    //COMBO
    public static List<clsComboItems.ComboItem> GetComboInstituciones()
    {
        return clsComboItems.GetItems("Exec sptInstitucionesSelectCombo");
    }
    //INSERT
    public clsNotificacion Insertar()
    {
        string query = "exec sptInstitucionesInsert '" + strNombreInstitucion + "','" + strLema + "','" + strLatitud + "','" + strLongitud + "','" + strAcercaDe + "','" + intPais + "','" + strDireccion + "','" + strTelefono + "','" + strCorreo + "','" + intPublica + "','" + strLinkFacebook + "','" + strLinkTwitter + "','" + strWeb + "','" + strFechaVencimiento + "','" + strUsuarioAlta + "'";
        return clsHerramientas.EjecutarQuery(query);
    }
    //Filtros
    public static List<InstitucionFiltro> GetInstitucionesFiltro(bool bolTop)
    {
        string strSql = "Exec sptInstitucionesSelectFiltros " + bolTop.ToString();
        DataTable dtInstituciones;
        dtInstituciones = clsHerramientas.GetDataTable(strSql);
        List<InstitucionFiltro> lstInstituciones = new List<InstitucionFiltro>();
        foreach (DataRow row in dtInstituciones.Rows)
        {
            lstInstituciones.Add(new InstitucionFiltro(row["InstitucionId"].ToString(), row["Nombre"].ToString(), row["Cursos"].ToString(), row["Pais"].ToString()));
        }
        return lstInstituciones;
    }
    public class InstitucionFiltro
    {

       public string strInstitucionId, strNombre, strCursos, strPais;

        public InstitucionFiltro(string InstitucionId, string Nombre, string Cursos, string Pais)
        {
            strInstitucionId = InstitucionId;
            strNombre = Nombre;
            strCursos = Cursos;
            strPais = Pais;
        }
    }
}