﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for clsComboItems
/// </summary>
public class clsComboItems
{
    public clsComboItems()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public class ComboItem
    {
        public string strValor, strDescripcion;
    }

    public static List<ComboItem> GetItems(string strSql)
    {
        List<ComboItem> lstItems = new List<ComboItem>();
        DataTable dtResult = clsHerramientas.GetDataTable(strSql);
        ComboItem objItem;
        foreach (DataRow row in dtResult.Rows)
        {
            objItem = new ComboItem();
            objItem.strValor = row["Valor"].ToString();
            objItem.strDescripcion = row["Nombre"].ToString();
            lstItems.Add(objItem);
        }
        return lstItems;
    }
    public class MisInstituciones
    {
        public string strNombre, strCategorias;
    }
    public static List<MisInstituciones> GetMisInstituciones(string strSql)
    {
        List<MisInstituciones> lstItems = new List<MisInstituciones>();
        DataTable dtResult = clsHerramientas.GetDataTable(strSql);
        MisInstituciones objItem;
        foreach (DataRow row in dtResult.Rows)
        {
            objItem = new MisInstituciones();
            objItem.strNombre = row["Nombre"].ToString();
            objItem.strCategorias = row["Categorias"].ToString();
            lstItems.Add(objItem);
        }
        return lstItems;
    }
    public class Aplicaciones
    {
        public string strTitulo, strStatus;
    }
    public static List<Aplicaciones> GetAplicaciones(string strSql)
    {
        List<Aplicaciones> lstItems = new List<Aplicaciones>();
        DataTable dtResult = clsHerramientas.GetDataTable(strSql);
        Aplicaciones objItem;
        foreach (DataRow row in dtResult.Rows)
        {
            objItem = new Aplicaciones();
            objItem.strTitulo = row["Titulo"].ToString();
            objItem.strStatus = row["Estatus"].ToString();
            lstItems.Add(objItem);
        }
        return lstItems;
    }
    public class MisProgramas
    {
        public string strNombre, strInstitucion;
    }
    public static List<MisProgramas> GetMisProgramas(string strSql)
    {
        List<MisProgramas> lstItems = new List<MisProgramas>();
        DataTable dtResult = clsHerramientas.GetDataTable(strSql);
        MisProgramas objItem;
        foreach (DataRow row in dtResult.Rows)
        {
            objItem = new MisProgramas();
            objItem.strNombre = row["Titulo"].ToString();
            objItem.strInstitucion = row["Institucion"].ToString();
            lstItems.Add(objItem);
        }
        return lstItems;
    }
}