﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for clsUsuario
/// </summary>
public class clsUsuario
{
    public int intUsuarioId;
    public string strNombreUsuario;
    public string strPassword;
    public string strEstatus;
    public string strNombre;
    public string strCorreo;
    public string strTelefono;
    public int intPais;
    public int intInstitucion;
    public string strNumeroControl;
    public string strFechaNacimiento;
    public string strFoto;
    public int intTipoUsuario;
    public int intGradoId;
    public int intUsuarioAlta;
    public string strFechaAlta;
    public int intUsuarioModifico;
    public string strFechaModifico;

    public clsUsuario()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public clsUsuario(Usuario objUsuario)
    {
        intUsuarioId = objUsuario.intUsuarioId;
        strNombreUsuario = objUsuario.strNombreUsuario;
        strEstatus = objUsuario.strEstatus;
        strNombre = objUsuario.strNombre;
        strCorreo = objUsuario.strCorreo;
        strTelefono = objUsuario.strTelefono;
        intPais = objUsuario.intPais;
        intInstitucion = objUsuario.intInstitucion;
        strNumeroControl = objUsuario.strNumeroControl;
        strFechaNacimiento = objUsuario.strFechaNacimiento;
        intTipoUsuario = objUsuario.intTipoUsuario;
    }

    public class Usuario
    {
        public int intUsuarioId;
        public string strNombreUsuario;
        public string strEstatus;
        public string strNombre;
        public string strCorreo;
        public string strTelefono;
        public int intPais;
        public int intInstitucion;
        public string strNumeroControl;
        public string strFechaNacimiento;
        public int intTipoUsuario;
        public int intGradoId;

        public UsuarioPerfil CargarPerfilUsuario()
        {
            string query = "sptUsuariosSelectPerfil " + intUsuarioId;

            DataTable tPerfil = clsHerramientas.GetDataTable(query);

            UsuarioPerfil objPerfil = new UsuarioPerfil();

            if (tPerfil.Rows.Count == 1)
            {
                //Perfil
                objPerfil.intUsuarioId = Convert.ToInt32(tPerfil.Rows[0]["UsuarioId"]);
                objPerfil.strNombreUsuario = tPerfil.Rows[0]["NombreUsuario"].ToString();
                objPerfil.strEstatus = tPerfil.Rows[0]["Estatus"].ToString();
                objPerfil.strNombre = tPerfil.Rows[0]["Nombre"].ToString();
                objPerfil.strCorreo = tPerfil.Rows[0]["Correo"].ToString();
                objPerfil.strTelefono = tPerfil.Rows[0]["Telefono"].ToString();
                objPerfil.strPais = tPerfil.Rows[0]["Pais"].ToString();
                objPerfil.strInstitucion = tPerfil.Rows[0]["NombreInstitucion"].ToString();
                objPerfil.strNumeroControl = tPerfil.Rows[0]["NumeroControl"].ToString();
                objPerfil.strFechaNacimiento = tPerfil.Rows[0]["FechaNacimiento"].ToString();
                objPerfil.strTipoUsuario = tPerfil.Rows[0]["TipoUsuario"].ToString();
                objPerfil.strGrado = tPerfil.Rows[0]["Grado"].ToString();
                objPerfil.MisInstituciones = GetMisInstituciones();
                objPerfil.Aplicaciones = GetAplicaciones();
                objPerfil.MisProgramas = GetMisProgramas();
            }


            return objPerfil;
        }

        public clsNotificacion ActualizarPerfilUsuario(Usuario usuario)
        {
            string query = "Exec sptUsuariosUpdatePerfil '" + usuario.intUsuarioId + "','" + usuario.strNombreUsuario + "','" + usuario.strNombre + "','" + usuario.strCorreo + "','" + usuario.strTelefono + "','" + usuario.intPais + "','" + usuario.strFechaNacimiento + "','" + usuario.intGradoId + "','" + usuario.intUsuarioId + "'";
            return clsHerramientas.EjecutarQuery(query);
        }

        public List<clsComboItems.MisInstituciones> GetMisInstituciones()
        {
            return clsComboItems.GetMisInstituciones("Exec sptInstitucionesSelectMisInstituciones " + intUsuarioId);
        }
        public List<clsComboItems.Aplicaciones> GetAplicaciones()
        {
            return clsComboItems.GetAplicaciones("Exec sptUsuariosProgramasGetAplicaciones " + intUsuarioId);
        }
        public List<clsComboItems.MisProgramas> GetMisProgramas()
        {
            return clsComboItems.GetMisProgramas("Exec sptProgramasResponsablesMisProgramas " + intUsuarioId);
        }
    }

    public class UsuarioPerfil
    {
        public int intUsuarioId;
        public string strNombreUsuario;
        public string strEstatus;
        public string strNombre;
        public string strCorreo;
        public string strTelefono;
        public string strPais;
        public string strInstitucion;
        public string strNumeroControl;
        public string strFechaNacimiento;
        public string strTipoUsuario;
        public string strGrado;
        public List<clsComboItems.MisInstituciones> MisInstituciones;
        public List<clsComboItems.Aplicaciones> Aplicaciones;
        public List<clsComboItems.MisProgramas> MisProgramas;
    }

    public clsNotificacion Insertar()
    {
        string query = "exec sptUsuariosInsert '" + strNombreUsuario + "', '" + strPassword + "', '" + strEstatus + "', '" + strNombre + "', '" + strCorreo +
                        "', '" + strTelefono + "', '" + intPais + "', '" + intInstitucion + "', '" + strNumeroControl + "', '" + strFechaNacimiento + "',' " + intTipoUsuario + "','" + intGradoId + "', '" + intUsuarioAlta + "'";
        return clsHerramientas.EjecutarQuery(query);
    }

    public static List<clsComboItems.ComboItem> GetComboTipoVinculacion(int intInstucionId)
    {
        return clsComboItems.GetItems("Exec sptUsuariosSelectComboVinculacion " + intInstucionId.ToString());
    }

    public Usuario IniciarSesion()
    {
        string query = "sptUsuariosInicioSesion '" + strNombreUsuario + "', '" + strPassword + "'";

        DataTable tdata = clsHerramientas.GetDataTable(query);

        Usuario UsuarioSesion = new Usuario();

        if (tdata.Rows.Count == 1)
        {
            UsuarioSesion.intUsuarioId = Convert.ToInt32(tdata.Rows[0]["UsuarioId"]);
            UsuarioSesion.strNombreUsuario = tdata.Rows[0]["NombreUsuario"].ToString();
            UsuarioSesion.strEstatus = tdata.Rows[0]["Estatus"].ToString();
            UsuarioSesion.strNombre = tdata.Rows[0]["Nombre"].ToString();
            UsuarioSesion.strCorreo = tdata.Rows[0]["Correo"].ToString();
            UsuarioSesion.strTelefono = tdata.Rows[0]["Telefono"].ToString();
            UsuarioSesion.intPais = Convert.ToInt32(tdata.Rows[0]["Pais"].ToString());
            UsuarioSesion.intInstitucion = Convert.ToInt32(tdata.Rows[0]["InstucionId"].ToString());
            UsuarioSesion.strNumeroControl = tdata.Rows[0]["NumeroControl"].ToString();
            UsuarioSesion.strFechaNacimiento = tdata.Rows[0]["FechaNacimiento"].ToString();
            UsuarioSesion.intTipoUsuario = Convert.ToInt32(tdata.Rows[0]["TipoUsurioId"].ToString());
            UsuarioSesion.intGradoId = Convert.ToInt32(tdata.Rows[0]["GradoId"].ToString());
        }

        return UsuarioSesion;
    }
    public static List<Usuario> getListaUsuarios(int intInstitucionId)
    {
        List<Usuario> lstUsuarios = new List<Usuario>();
        DataTable dt = new DataTable();
        string sql = "Select UsuarioId,NombreUsuario,Nombre,Correo,Telefono,InstucionId,Foto,TipoUsurioId FROM tUsuarios where InstucionId=" + intInstitucionId +
            " and  TipoUsurioId=2";

        clsNotificacion objNotificacion = new clsNotificacion();
        dt = clsHerramientas.GetDataTable(sql);
        Usuario objUsuarios;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            objUsuarios = new Usuario();
            objUsuarios.intUsuarioId = int.Parse(dt.Rows[i]["UsuarioId"].ToString());
            objUsuarios.strNombreUsuario = dt.Rows[i]["NombreUsuario"].ToString();
            objUsuarios.strNombre = dt.Rows[i]["Nombre"].ToString();
            objUsuarios.strCorreo = dt.Rows[i]["Correo"].ToString();
            objUsuarios.strTelefono = dt.Rows[i]["Telefono"].ToString();
            objUsuarios.intInstitucion = int.Parse(dt.Rows[i]["InstucionId"].ToString());
            //GetImagen
            //objUsuarios.strFoto = dt.Rows[i]["Foto"].ToString();
            objUsuarios.intTipoUsuario = int.Parse(dt.Rows[i]["TipoUsurioId"].ToString());

            lstUsuarios.Add(objUsuarios);
        }
        return lstUsuarios;
    }
}
