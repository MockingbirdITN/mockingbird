﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for clsPrograma
/// </summary>
public class clsPrograma
{

    public int intProgramaId;
    public int intInstitucionId;
    public int intTipoProgramaId;
    public string strTitulo;
    public string strDescripcionCorta;
    public string strDescripcionLarga;
    public string strPalabrasClave;
    public string strRequisitos;
    public string strFechaInicio;
    public string strFechaFinal;
    public int intUsuarioAlta;
    public string strFechaAlta;
    public int intUsuarioModifico;
    public string strFechaModifico;

    public class Programa
    {
        public int intProgramaId;
        public int intInstitucionId;
        public int intTipoProgramaId;
        public string strTitulo;
        public string strDescripcionCorta;
        public string strDescripcionLarga;
        public string strPalabrasClave;
        public string strRequisitos;
        public string strFechaInicio;
        public string strFechaFinal;
        public int intUsuarioAlta;
        public string strFechaAlta;
        public int intUsuarioModifico;
        public string strFechaModifico;

        public string strNombreInstitucion;
        public string strTipoPrograma;
    }

    public class ProgramaPerfil
    {

        public string ProgramaId;
        public string Titulo;
        public string DescripcionLarga;
        public string NombreInstitucion;
        public string TipoPrograma;
        public string FechaInicio;
        public string FechaFinal;
        public string Requisitos;
        public List<string> lstCategorias;
        public List<string> lstGrados;

    }

    public static ProgramaPerfil GetProgramaPerfil(int intProgramaId)
    {
        string strSql = "SELECT * FROM vwProgramaPerfil WHERE ProgramaId = " + intProgramaId.ToString();
        DataTable tPerfil = clsHerramientas.GetDataTable(strSql);

        ProgramaPerfil objPerfil = new ProgramaPerfil();

        if (tPerfil.Rows.Count > 0) {
            //Perfil
            objPerfil.ProgramaId = tPerfil.Rows[0]["ProgramaId"].ToString();
            objPerfil.Titulo = tPerfil.Rows[0]["Titulo"].ToString();
            objPerfil.DescripcionLarga = tPerfil.Rows[0]["DescripcionLarga"].ToString();
            objPerfil.NombreInstitucion = tPerfil.Rows[0]["NombreInstitucion"].ToString();
            objPerfil.TipoPrograma = tPerfil.Rows[0]["TipoPrograma"].ToString();
            objPerfil.FechaInicio = tPerfil.Rows[0]["FechaInicio"].ToString();
            objPerfil.FechaFinal = tPerfil.Rows[0]["FechaFinal"].ToString();
            objPerfil.Requisitos = tPerfil.Rows[0]["Requisitos"].ToString();
            //Grados
            strSql = "SELECT * FROM vwProgramaGrados WHERE ProgramaId = " + intProgramaId.ToString();
            DataTable tGrados = clsHerramientas.GetDataTable(strSql);
            objPerfil.lstGrados = new List<string>();
            foreach (DataRow row in tGrados.Rows)
            {
                objPerfil.lstGrados.Add(row["Grado"].ToString());
            }
            //Categorias
            strSql = "SELECT * FROM vwProgramaCategorias WHERE ProgramaId = " + intProgramaId.ToString();
            DataTable tCategorias = clsHerramientas.GetDataTable(strSql);
            objPerfil.lstCategorias = new List<string>();
            foreach (DataRow row in tCategorias.Rows)
            {
                objPerfil.lstCategorias.Add(row["Categoria"].ToString());
            }
        }

        return objPerfil;

    }

    public static clsNotificacion Insertar(Programa objPrograma)
    {
        string query = "exec sptProgramasInsert " + objPrograma.intInstitucionId + ", " + objPrograma.intTipoProgramaId + ", '" + objPrograma.strTitulo + "', '" +
                            objPrograma.strDescripcionCorta + "', '" + objPrograma.strDescripcionLarga + "', '" + objPrograma.strPalabrasClave + "', '" + objPrograma.strRequisitos + "', '" + objPrograma.strFechaInicio + 
                            "', '" + objPrograma.strFechaFinal + "', '" + objPrograma.intUsuarioAlta + "'";

        DataTable dtPrograma;
        dtPrograma = clsHerramientas.GetDataTable(query);
        clsNotificacion objNotificacion = new clsNotificacion();
        if (dtPrograma.Rows.Count > 0) {
            objNotificacion.intCodigo = 0;
            objNotificacion.strMensaje = dtPrograma.Rows[0][0].ToString();
        }

        return objNotificacion;
    }

    public clsNotificacion Actualizar()
    {
        string query = "exec sptProgramasUpdate values (@ProgramaId= " + intProgramaId + ",@InstitucionId= " + intInstitucionId + ",@TipoProgramaId= " + intTipoProgramaId + ",@Titulo= '" + strTitulo + "',@DescripcionCorta= '" + strDescripcionCorta + "',@DescripcionLarga= '" + strDescripcionLarga + "',@PalabrasClave= '" + strPalabrasClave + "',@Requisitos= '" + strRequisitos + "',@FechaInicio= '" + strFechaInicio + "',@FechaFinal= '" + strFechaFinal + "',@UsuarioAlta= " + intUsuarioAlta + ",@FechaAlta= '" + strFechaAlta + "',@UsuarioModifico= " + intUsuarioModifico + ",@FechaModifico= '" + strFechaModifico + "')";
        return clsHerramientas.EjecutarQuery(query);
    }

    public static clsNotificacion Eliminar(int intProgramaId)
    {
        string query = "exec sptProgramasDelete values ( @ProgramaId = " + intProgramaId + ")";
        return clsHerramientas.EjecutarQuery(query);
    }

    public static Programa getPrograma(int intProgramaId)
    {
        DataTable dt = new DataTable();
        Programa objProgramas = new Programa();
        string query = "select ProgramaId,InstitucionId,TipoProgramaId,Titulo,DescripcionCorta,DescripcionLarga,PalabrasClave,Requisitos,FechaInicio,FechaFinal,UsuarioAlta,FechaAlta,UsuarioModifico,FechaModifico FROM tProgramas where ProgramaId=" + intProgramaId + "";
        clsNotificacion objNotificacion = new clsNotificacion();
        dt = clsHerramientas.GetDataTable(query);
        if (dt.Rows.Count > 0)
        {
            objProgramas.intProgramaId = int.Parse(dt.Rows[0]["ProgramaId"].ToString());
            objProgramas.intInstitucionId = int.Parse(dt.Rows[0]["InstitucionId"].ToString());
            objProgramas.intTipoProgramaId = int.Parse(dt.Rows[0]["TipoProgramaId"].ToString());
            objProgramas.strTitulo = dt.Rows[0]["Titulo"].ToString();
            objProgramas.strDescripcionCorta = dt.Rows[0]["DescripcionCorta"].ToString();
            objProgramas.strDescripcionLarga = dt.Rows[0]["DescripcionLarga"].ToString();
            objProgramas.strPalabrasClave = dt.Rows[0]["PalabrasClave"].ToString();
            objProgramas.strRequisitos = dt.Rows[0]["Requisitos"].ToString();
            objProgramas.strFechaInicio = dt.Rows[0]["FechaInicio"].ToString();
            objProgramas.strFechaFinal = dt.Rows[0]["FechaFinal"].ToString();
            objProgramas.intUsuarioAlta = int.Parse(dt.Rows[0]["UsuarioAlta"].ToString());
            objProgramas.strFechaAlta = dt.Rows[0]["FechaAlta"].ToString();
            objProgramas.intUsuarioModifico = int.Parse(dt.Rows[0]["UsuarioModifico"].ToString());
            objProgramas.strFechaModifico = dt.Rows[0]["FechaModifico"].ToString();
        }
        return objProgramas;
    }

    public static List<Programa> getListaProgramas()
    {
        List<Programa> lstProgramas = new List<Programa>();
        DataTable dt = new DataTable();
        string sql = "SelectProgramaId,InstitucionId,TipoProgramaId,Titulo,DescripcionCorta,DescripcionLarga,PalabrasClave,Requisitos,FechaInicio,FechaFinal,UsuarioAlta,FechaAlta,UsuarioModifico,FechaModifico FROM tProgramas";

        clsNotificacion objNotificacion = new clsNotificacion();
        dt = clsHerramientas.GetDataTable(sql);
        Programa objProgramas;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            objProgramas = new Programa();
            objProgramas.intProgramaId = int.Parse(dt.Rows[i]["ProgramaId"].ToString());
            objProgramas.intInstitucionId = int.Parse(dt.Rows[i]["InstitucionId"].ToString());
            objProgramas.intTipoProgramaId = int.Parse(dt.Rows[i]["TipoProgramaId"].ToString());
            objProgramas.strTitulo = dt.Rows[i]["Titulo"].ToString();
            objProgramas.strDescripcionCorta = dt.Rows[i]["DescripcionCorta"].ToString();
            objProgramas.strDescripcionLarga = dt.Rows[i]["DescripcionLarga"].ToString();
            objProgramas.strPalabrasClave = dt.Rows[i]["PalabrasClave"].ToString();
            objProgramas.strRequisitos = dt.Rows[i]["Requisitos"].ToString();
            objProgramas.strFechaInicio = dt.Rows[i]["FechaInicio"].ToString();
            objProgramas.strFechaFinal = dt.Rows[i]["FechaFinal"].ToString();
            objProgramas.intUsuarioAlta = int.Parse(dt.Rows[i]["UsuarioAlta"].ToString());
            objProgramas.strFechaAlta = dt.Rows[i]["FechaAlta"].ToString();
            objProgramas.intUsuarioModifico = int.Parse(dt.Rows[i]["UsuarioModifico"].ToString());
            objProgramas.strFechaModifico = dt.Rows[i]["FechaModifico"].ToString();

            lstProgramas.Add(objProgramas);
        }
        return lstProgramas;
    }

    public static List<Programa> getListaProgramasInstitucion(int intInstitucionId)
    {
        List<Programa> lstProgramas = new List<Programa>();
        DataTable dt = new DataTable();
        string sql = "exec sptProgramasRelacionadosSelect " + intInstitucionId;

        clsNotificacion objNotificacion = new clsNotificacion();
        dt = clsHerramientas.GetDataTable(sql);
        Programa objProgramas;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            objProgramas = new Programa();
            objProgramas.intProgramaId = int.Parse(dt.Rows[i]["ProgramaId"].ToString());
            objProgramas.intInstitucionId = int.Parse(dt.Rows[i]["InstitucionId"].ToString());
            objProgramas.intTipoProgramaId = int.Parse(dt.Rows[i]["TipoProgramaId"].ToString());
            objProgramas.strTitulo = dt.Rows[i]["Titulo"].ToString();
            objProgramas.strDescripcionCorta = dt.Rows[i]["DescripcionCorta"].ToString();
            objProgramas.strNombreInstitucion = dt.Rows[i]["NombreInstitucion"].ToString();
            objProgramas.strTipoPrograma = dt.Rows[i]["TipoPrograma"].ToString();
            //objProgramas.strFechaModifico = dt.Rows[i]["Imagenprograma"].ToString();
            //objProgramas.strFechaModifico = dt.Rows[i]["LogoInstitucion"].ToString();

            lstProgramas.Add(objProgramas);
        }
        return lstProgramas;
    }
    public static List<Programa> GetUltimosProgramas()
    {
        List<Programa> lstProgramas = new List<Programa>();
        DataTable dt = new DataTable();
        string sql = "exec sptProgramasUltimosSelect";

        clsNotificacion objNotificacion = new clsNotificacion();
        dt = clsHerramientas.GetDataTable(sql);
        Programa objProgramas;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            objProgramas = new Programa();
            objProgramas.intProgramaId = int.Parse(dt.Rows[i]["ProgramaId"].ToString());
            objProgramas.intInstitucionId = int.Parse(dt.Rows[i]["InstitucionId"].ToString());
            objProgramas.intTipoProgramaId = int.Parse(dt.Rows[i]["TipoProgramaId"].ToString());
            objProgramas.strTitulo = dt.Rows[i]["Titulo"].ToString();
            objProgramas.strDescripcionCorta = dt.Rows[i]["DescripcionCorta"].ToString();
            objProgramas.strNombreInstitucion = dt.Rows[i]["NombreInstitucion"].ToString();
            objProgramas.strTipoPrograma = dt.Rows[i]["TipoPrograma"].ToString();
            //objProgramas.strFechaModifico = dt.Rows[i]["Imagenprograma"].ToString();
            //objProgramas.strFechaModifico = dt.Rows[i]["LogoInstitucion"].ToString();

            lstProgramas.Add(objProgramas);
        }
        return lstProgramas;
    }
}