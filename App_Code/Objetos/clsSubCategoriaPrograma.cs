﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for clsSubCategoriaPrograma
/// </summary>
public class clsSubCategoriaPrograma
{
    public clsSubCategoriaPrograma()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public int intCategoriaId;
    public string strDescripcion;


    public class SubCategoriaPrograma
    {

        public int intCategoriaId, SubCategoriaId;
        public string strDescripcion;

    }
    public class AuxIndexCategoriaSubCategoria
    {

        public int intId, intTipo;
        public string strDescripcion, strIcono;

    }

    public void Insertar()
    {

        // string strSql = "INSERT INTO tPais(Pais, Descripcion) VALUES ('" + strPais + "', '" + strDescripcion + "')";

    }

    //public static SubCategoriaPrograma GetSubCategoriaPrograma(int CategoriaId)
    //{

    //    //string strSql = "SELECT * FROM tCategoriasProgramas WHERE CategoriaID = " + CategoriaId.ToString();
    //    //SubCategoriaPrograma objCategoria = new SubCategoriaPrograma();
    //    //objCategoria.intCategoriaId = CategoriaId;
    //    //objCategoria.strDescripcion = "Servidor " + CategoriaId.ToString();

    //    //return objCategoria;

    //}

    public static List<clsComboItems.ComboItem> GetCombo(int intCategoriaId)
    {
        return clsComboItems.GetItems("Exec sptSubCategoriasSelectCombo " + intCategoriaId.ToString());
    }

    public static List<AuxIndexCategoriaSubCategoria> CargarCategoriasIndex()
    {
        List<AuxIndexCategoriaSubCategoria> lstCategorias = new List<AuxIndexCategoriaSubCategoria>();
        DataTable dt = new DataTable();
        string sql = "exec sptSubcategoriasCategoriasPrograma ";

        clsNotificacion objNotificacion = new clsNotificacion();
        dt = clsHerramientas.GetDataTable(sql);
        AuxIndexCategoriaSubCategoria objAuxIndexCategoriaSubCategoria;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            objAuxIndexCategoriaSubCategoria = new AuxIndexCategoriaSubCategoria();
            objAuxIndexCategoriaSubCategoria.intId = int.Parse(dt.Rows[i]["id"].ToString());
            objAuxIndexCategoriaSubCategoria.intTipo = int.Parse(dt.Rows[i]["tipo"].ToString());
            objAuxIndexCategoriaSubCategoria.strDescripcion = dt.Rows[i]["descripcion"].ToString();
            objAuxIndexCategoriaSubCategoria.strIcono = dt.Rows[i]["icono"].ToString();

            lstCategorias.Add(objAuxIndexCategoriaSubCategoria);
        }
        return lstCategorias;
    }


}