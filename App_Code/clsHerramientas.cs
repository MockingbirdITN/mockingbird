﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for Class1
/// </summary>
public class clsHerramientas
{
    public clsHerramientas()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    public static bool EnviarMail(string strSMTP, string strUsuario, string strPassword, string strMensaje, string strSubject, List<string> lstMail)
    {

        MailMessage mail = new MailMessage();
        try
        {
            mail.From = new MailAddress("MockingbirdITN@gmail.com");
        }
        catch (Exception)
        {
            return false;
        }

        foreach (string strMail in lstMail)
        {
            try
            {
                mail.To.Add(strMail);
            }
            catch (Exception)
            {

                throw;
            }

        }
        //No hay correos para enviar
        if (mail.To.Count == 0)
        {
            return false;
        }

        mail.Subject = strSubject;
        mail.Body = strMensaje;
        mail.IsBodyHtml = true;

        SmtpClient smtpServer = new SmtpClient(strSMTP);
        //smtpServer.Port = 587;

        smtpServer.Credentials = new System.Net.NetworkCredential(strUsuario, strPassword);
        //smtpServer.EnableSsl = true;
        //smtpServer.Timeout = 10000;
        try
        {
            smtpServer.Send(mail);
        }
        catch (SmtpException ex)
        {

            return false;
        }
        return true;
    }
    public static DataTable GetDataTable(string strSql)
    {

        DataTable dtTemp = new DataTable();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlDataAdapter da = new SqlDataAdapter(strSql, con);
        try
        {
            da.Fill(dtTemp);
        }
        catch (Exception ex)
        {
        }

        return dtTemp;

    }

    public static  clsNotificacion EjecutarQuery(string strSql){
    
     DataTable dtTemp = new DataTable();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlCommand cmd = new SqlCommand(strSql, con);
        clsNotificacion objNotificacion = new clsNotificacion();
        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            objNotificacion.intCodigo = 1;
        }
        catch (SqlException ex)
        {
            objNotificacion.intCodigo = 0;

            objNotificacion.strMensaje =ex.Message;
        }

        return objNotificacion;
    
    }

}

