/// <reference path="../../Scripts/jquery-1.12.4.min.js" />
/// <reference path="../../js/Herramientas.js" />

$(document).ready(function () {
    loadPage_Annon();
    CargarFiltros();

    $("#btnMostrarInstituciones").click(function () {
        MostrarInstituciones();
        return false;
    });

});


function CargarFiltros() {

    CargarInstituciones(true);
    return false;
}

function CargarInstituciones(bolTop) {
    //Cargar top 10 de instituciones
    AjaxPost("../../wsObjetos.asmx/GetInstitucionesFiltro", "'bolTop':" + bolTop, true, function (lstInstituciones) {
        var strInstitucion = "";
        var objInstitucion;
        var objDivInstituciones = $("#divInstituciones");
        for (var i = 0; i < lstInstituciones.length; i++) {
            objInstitucion = lstInstituciones[i];
            strInstitucion = "<div class='checkbox-block'> " +
                                "<input id='property_type-" + i + "' data-institucionid='" + objInstitucion.strInstitucionId + "' type='checkbox' class='checkbox'> " +
                                "<label class='' for='property_type-" + i + "'>" + objInstitucion.strNombre + "<span class='checkbox-count'>(" + objInstitucion.strCursos + ")</span></label> " +
                             "</div>"
            objDivInstituciones.append(strInstitucion);
        }
        return false;
    });
    return false;
}

function MostrarInstituciones() {
    var objConfiguracion = {
        strTitulo:"Seleccionar Instituciones",
        strBody: "<div id='divFiltroInstituciones' style='height:250px; overflow:auto'></div>",
        strConfirmar: "",
        fnConfirmar: function () { },
        fnCancelar: function () { }
    };
    ModalConfirmBootstrap(objConfiguracion);
    //var objDivInstituciones = $("#divFiltroInstituciones");
    ////Cargar top 10 de instituciones
    //AjaxPost("../../wsObjetos.asmx/GetInstitucionesFiltro", "'bolTop':" + false, true, function (lstInstituciones) {
    //    var strInstitucion = "";
    //    var objInstitucion;
    //    var objPais;
    //    for (var i = 0; i < lstInstituciones.length; i++) {
    //        objInstitucion = lstInstituciones[i];
    //        objPais = objDivInstituciones.find("#" + objInstitucion.strPais)[0];
    //        if (objPais === undefined) {
    //            objDivInstituciones.append("<div id='" + objInstitucion.strPais + "'><div class='row'></div></div>");
    //            objPais = ("#" + objInstitucion.strPais);
    //        }

    //        strInstitucion = "<div class='col-md-4 checkbox-block'> " +
    //                            "<input id='property_type-" + i + "' data-institucionid='" + objInstitucion.strInstitucionId + "' type='checkbox' class='checkbox'> " +
    //                            "<label class='' for='property_type-" + i + "'>" + objInstitucion.strNombre + "<span class='checkbox-count'>(" + objInstitucion.strCursos + ")</span></label> " +
    //                        "</div>"
    //        //Si aun no se acompletan 4 por row append en ultimo
    //        if (objPais.find(".row:last").children().length < 4) {

    //            objPais.find(".row:last").append(strInstitucion);
    //        }
    //        else {//Agregar nuevo row y append
    //            objPais.append("<div class='row'>" + strInstitucion + "</div>");
    //        }


    //    }
    //    return false;
    //});

    return false;
}