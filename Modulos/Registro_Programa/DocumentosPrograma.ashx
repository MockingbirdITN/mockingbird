﻿<%@ WebHandler Language="C#" Class="DocumentosPrograma" %>

using System;
using System.Web;
using System.Data.SqlClient;
public class DocumentosPrograma : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        string strFileName, strFileExtension, strSql, strTipo;
        long lgContenido = 0;
        byte[] arrBuffer = new byte[0];
        string strProgramaId = context.Request.QueryString["intPrograma"];
        string strUsuario = context.Request.QueryString["strUsuario"];
        string[] strArchivoSplit;
        foreach (string s in context.Request.Files)
        {
            HttpPostedFile file = context.Request.Files[s];
            //  int fileSizeInBytes = file.ContentLength;
            strFileName = file.FileName;
            strFileExtension = "xxx";
            strArchivoSplit = file.FileName.Split('.');
            if (strArchivoSplit.Length > 0) {
                strFileExtension = strArchivoSplit[1];
            
            }
            
            lgContenido = file.InputStream.Length;
            //byte[] arrBuffer;
            Array.Resize(ref arrBuffer, (int)lgContenido);
            Array.Clear(arrBuffer, 0, arrBuffer.Length);
            file.InputStream.Read(arrBuffer, 0, (int)lgContenido);
            strTipo = "D";
            switch (strFileName.ToUpper().Trim())
            {
                case "BANNER.PNG":
                    strTipo = "B";
                    break;
                case "BUSQUEDA.PNG":
                    strTipo = "S";
                    break;
            }

            if (!string.IsNullOrEmpty(strFileName))
            {

                strSql = "INSERT INTO tArchivosPrograma (ProgramaId,Descripcion,Nombre,TipoArchivo,Archivo,ExtensionArchivo, " +
                            " UsuarioAlta,FechaAlta,UsuarioModifico,FechaModifico) VALUES (" + strProgramaId + ", '', '" + strFileName + "', '" + strTipo + "', " +
                            " @Archivo, '" + strFileExtension + "', '" + strUsuario + "', GETDATE(), '" + strUsuario + "', GETDATE())";

                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                SqlCommand cmd = new SqlCommand(strSql, con);

                SqlParameter sqpParametro = new SqlParameter("@Archivo", System.Data.SqlDbType.Image, arrBuffer.Length, "Archivo");

                sqpParametro.Size = (int)lgContenido;
                sqpParametro.Value = arrBuffer;

                cmd.Parameters.Add(sqpParametro);

                try
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    //Me.LbL_Mensaje.Text = "Archivo Agregado"
                }
                catch (SqlException ex)
                {
                    clsNotificacion objNotificacion = new clsNotificacion();
                    objNotificacion.intCodigo = 1;
                    objNotificacion.strMensaje = ex.Message;
                    context.Response.Write("Error al cargar imagen: " + ex.Message);
                    context.Response.End();
                }

                context.Response.Write("");

            }
        }




    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}