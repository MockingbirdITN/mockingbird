/// <reference path="../../js/jquery-2.2.4.min.js" />


$(document).ready(function () {
    loadPage_Annon();


    //Inicializar combos
    CargarTipoProgramas();
    CargarCategorias();
    CargarResponsables();
    //Subcategoria -- cascada
    $("#cbSubCategorias").select2();
    $("#cbCategorias").change(function () {
        CargarSubCategorias();
        return false;
    });
    //Crear programa
    $("#btnCrear").click(function () {
        CrearPrograma();
        return false;
    });
    Dropzone.autoDiscover = false;
    $("#dZUpload").dropzone({
        url: "DocumentosPrograma.ashx",
        addRemoveLinks: true,
        success: function (file, response) {
            //objRespuesta = JSON.parse(response);
            if (response !== "") {
                $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response);
            }
            else {
                file.previewElement.classList.add("dz-success");
            }
        },
        error: function (file, response) {
            //file.previewElement.classList.add("dz-error");
            $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response);
        },
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 10
    });

});

function CargarTipoProgramas() {
    $("#cbTipoPrograma").select2();
    AjaxPost("../../wConsultaCombos.asmx/CargarTipoProgramas", "", false, function (lstItems) {
        CargarCombo($("#cbTipoPrograma"), lstItems);

        return false;
    });
    return false;
}

function CargarCategorias() {
    $("#cbCategorias").select2();
    AjaxPost("../../wConsultaCombos.asmx/CargarCategorias", "", false, function (lstItems) {
        CargarCombo($("#cbCategorias"), lstItems);

        return false;
    });
    return false;
}

function CargarSubCategorias() {

    AjaxPost("../../wConsultaCombos.asmx/CargarSubCategorias", "'intCategoriaId':" + $("#cbCategorias").val(), false, function (lstItems) {
        CargarCombo($("#cbSubCategorias"), lstItems);

        return false;
    });
    return false;
}

function CargarResponsables() {
    $("#cbResponsables").select2();
    AjaxPost("../../wConsultaCombos.asmx/CargarUsuariosVinculacion", "'intInstitucionId':" + GetInstitucionId(), false, function (lstItems) {
        CargarCombo($("#cbResponsables"), lstItems);

        return false;
    });
    return false;
}


function GetProgramaCliente() {

    return {

        intProgramaId: 0,
        intInstitucionId: GetInstitucionId(),
        intTipoProgramaId: $("#cbTipoPrograma").val(),
        strTitulo: $("#txtTitulo").val(),
        strDescripcionCorta: $("#txtDescripcionCorta").val(),
        strDescripcionLarga: $("#txtDescripcionLarga").val(),
        strPalabrasClave: $("#txtPalabrasClave").val(),
        strRequisitos: $("#txtRequisitos").val(),
        strFechaInicio: $("#txtFechaInicio").val(),
        strFechaFinal: $("#txtFechaFinal").val(),
        intUsuarioAlta: GetUsuarioId(),
        //  strFechaAlta
        intUsuarioModifico: GetUsuarioId()
        //  strFechaModifico

    };

}

function CrearPrograma() {

    AjaxPost("../../Perfiles/wsPerfiles.asmx/InsertarPrograma", "'objPrograma':" + JSON.stringify(GetProgramaCliente()), false, function (objNotificacion) {

        if (objNotificacion.intCodigo == 1) {
        }
        else {
            intProgramaId = objNotificacion.strMensaje;
            var myDropzone = Dropzone.forElement(".dropzone");
            myDropzone.options.url = "DocumentosPrograma.ashx?intPrograma=" + objNotificacion.strMensaje + "&strUsuario=" + GetUsuarioId();
            myDropzone.processQueue();
        }

        return false;
    });


    return false;

}