///// <reference path="../../js/Herramientas.js" />
///// <reference path="../../js/jquery-2.2.4.min.js" />
///// <reference path="" />

$(document).ready(function () {
    loadPage_Annon();

    CargarPrograma();
    CargarCursosRelacionados();

    return false;
});


function CargarPrograma() {

    var intProgramaId = Cookies.get("programaid");
    //var intProgramaId = 1;

    AjaxPost("../wsPerfiles.asmx/GetProgramaPerfil", "'intProgramaId':" + intProgramaId, true, function (objPrograma) {
       //Perfil
        $("#txtTitulo").html(objPrograma.Titulo);
        $("#txtDescripcionLarga").html(objPrograma.DescripcionLarga);
        $("#txtNombreInstitucion").html(objPrograma.NombreInstitucion);
        $("#txtTipoPrograma").html(objPrograma.TipoPrograma);
        $("#txtFechaInicio").html(objPrograma.FechaInicio);
        $("#txtFechaFinal").html(objPrograma.FechaFinal);
        $("#txtRequisitos").html(objPrograma.Requisitos);
        //Categorias
        for (var i = 0; i < objPrograma.lstCategorias.length; i++) {
    
            $("#txtProgramasCategorias").append(objPrograma.lstCategorias[i] + ";");

        }
        //Grados
        for (var i = 0; i < objPrograma.lstGrados.length; i++) {
    
            $("#txtProgramasGrados").append(objPrograma.lstGrados[i]);

        }

        return false;

    });
 

    return false;

}

function CargarCursosRelacionados() {

    
    var Json = [{ ImagenPrograma: "ruta", Logo: "logo", Nombre: "Nombre", Titulo: "Titulo", DescripcionCorta: "Descripicon" }];

    HtmlLoadHandleBars($("#Related_Courses_List"), "../../templates/Courses.html", "Courses", Json);
    return false;

}