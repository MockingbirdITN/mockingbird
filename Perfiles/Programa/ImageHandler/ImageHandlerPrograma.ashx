﻿<%@ WebHandler Language="C#" Class="ImageHandler" %>

using System;
using System.Web;
using System.Data;

public class ImageHandler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {

        string strTipo = context.Request.QueryString["Tipo"];
        string strProgramaId = context.Request.QueryString["ProgramaId"];


        string strSql = "SELECT TOP 1 * FROM tArchivosPrograma WHERE ProgramaId = '" + strProgramaId + "' AND TipoArchivo = '" + strTipo + "'";
        DataTable dtArchivos = clsHerramientas.GetDataTable(strSql);

        if (dtArchivos.Rows.Count > 0)
        {
            switch (dtArchivos.Rows[0]["ExtensionArchivo"].ToString())
            {
                case "JPG":
                    context.Response.ContentType = "image/jpeg";
                    break;
                case "JPEG":
                    context.Response.ContentType = "image/jpeg";
                    break;
                case "BMP":
                    context.Response.ContentType = "image/bmp";
                    break;
                case "PNG":
                    context.Response.ContentType = "image/png";
                    break;
                case "PDF":
                    context.Response.ContentType = "application/pdf";
                    break;

            }
            context.Response.BinaryWrite((byte[])dtArchivos.Rows[0]["Archivo"]);

        }


    }

}