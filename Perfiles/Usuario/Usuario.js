$(document).ready(function () {
    loadPage_User();
    CargarPerfilUsuario();
});

var id = 0, perfil;

function CargarPerfilUsuario() {
    AjaxPost("../wsPerfiles.asmx/CargarUsuarioPerfil", "", false, function (Perfil) {
        id = Perfil['intUsuarioId'];
        $('#txtNombre').text(Perfil['strNombre']);
        $('#txtInstitucion').text(Perfil['strInstitucion']);
        $('#txtPais').append(Perfil['strPais']);
        $('#txtTelefono').append(Perfil['strTelefono']);
        $('#txtCorreo').append(Perfil['strCorreo']);
        $('#txtFechaNacimiento').append(Perfil['strFechaNacimiento']);

        HtmlLoadHandleBars($('#MisInstituciones_List'), '../../templates/User_Profile.html', 'Institution_li', Perfil['MisInstituciones']);
        HtmlLoadHandleBars($('#Aplicaciones_List'), '../../templates/User_Profile.html', 'Application_li', Perfil['Aplicaciones']);
        HtmlLoadHandleBars($('#MisProgramas_List'), '../../templates/User_Profile.html', 'Program_li', Perfil['MisProgramas']);

        if (Perfil['strPais'] === "") {
            ModalEditarPerfil(Perfil);
        }
        perfil = Perfil;
    });
}

$('#Btn_Editar').click(function () {
    ModalEditarPerfil(perfil);
});

function ModalEditarPerfil(Perfil) {
    $modalForgotPassword.modal('hide');
    $modalRegister.modal('hide');

    $('body').modalmanager('loading');

    setTimeout(function () {
        $modal.load('../../templates/ajax-modal-userinfo.html', '', function () {
            $modal.modal();

            AjaxPost("../../wConsultaCombos.asmx/CargarPaises", "", false, function (lstItems) {
                CargarCombo($("#cmbPais_Modal"), lstItems);
            });

            AjaxPost("../../wConsultaCombos.asmx/CargarGrados", "", false, function (lstItems) {
                CargarCombo($("#cmbGrado_Modal"), lstItems);
            });

            $('#txtNombre_Modal').val(Perfil['strNombre']);
            $('#txtCorreo_Modal').val(Perfil['strCorreo']);
            $('#txtTelefono_Modal').val(Perfil['strTelefono']);
            $('#cmbPais_Modal').val($('#cmbPais_Modal option:contains(' + Perfil['strPais'] + ')')).trigger('change');
            $('#cmbGrado_Modal').val($('#cmbGrado_Modal option:contains(' + Perfil['strGrado'] + ')')).trigger('change');
            $('#txtFechaNacimiento_Modal').val(Perfil['strFechaNacimiento']);
            $('#txtNombreUsuario_Modal').val(Perfil['strNombreUsuario']);

            $('#btn_Actualizar').click(function () {
                ActualizarPerfilUsuario();
            });

        });
    }, 1000);
}

function ActualizarPerfilUsuario() {
    var ActualizacionUsuarioPerfil = {
        UsuarioId: id,
        NombreUsuario: $("#txtNombreUsuario_Modal").val(),
        Nombre: $("#txtNombre_Modal").val(),
        Correo: $("#txtCorreo_Modal").val(),
        Telefono: $("#txtTelefono_Modal").val(),
        Pais: $("#cmbPais_Modal").val(),
        FechaNacimiento: $("#txtFechaNacimiento_Modal").val(),
        GradoId: $("#cmbGrado_Modal").val()
    };
    AjaxPost("../wsPerfiles.asmx/ActualizarUsuarioPerfil", "'ActualizacionUsuarioPerfil':" + JSON.stringify(ActualizacionUsuarioPerfil), false, function (boolResult) {
        window.location.reload();
    });
    return false;
}