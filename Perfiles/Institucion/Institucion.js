/// <reference path="../../js/jquery-2.2.4.min.js" />

$(document).ready(function () {
    if (CheckSesion() === 0) {
        $('#btnAlerta').remove();
    }
    CargarInstitucion();
});


function CargarInstitucion() {

    //var intInstitucionId = Cookie.get("intInstitucionId");
    var intInstitucionId = 1;

    AjaxPost("../wsPerfiles.asmx/GetInstitucionPerfil", "'intInstitucionId':" + intInstitucionId, true, function (objInstitucion) {

        //public string InstitucionId, NombreInstitucion, Lema, Latitud, Longitud,
        //    , Pais, Direccion, Telefono, Correo, Publica, LinkFacebook, LinkTwitter, Web, Logo, FechaVencimiento,
        //    UsuarioAlta, FechaAlta, UsuarioModifico, FechaModifico, Estatus;

        //Perfil 
        $("#txtNombre").html(objInstitucion.NombreInstitucion);
        $("#txtLema").html(objInstitucion.Lema);

        $("#txtDireccionPais").append(objInstitucion.Direccion + " " + objInstitucion.Pais);
        $("#txtTelefono").append(objInstitucion.Telefono);
        $("#txtCorreo").append(objInstitucion.Correo);
        $("#txtAcercaDe").text(objInstitucion.AcercaDe);
        CargarListaProgramas(intInstitucionId);
        CargarListaUsuarios(intInstitucionId);

        //Links remover boton si no cuenta con f/t/w
        var objLinkFace = $("#txtLinkFacebook");
        if (objInstitucion.LinkFacebook !== "") {

            $(objLinkFace).click(function () {
                window.open( "http://" + objInstitucion.LinkFacebook,'_blank');
                return false;
            });
        }
        else {
            $(objLinkFace).remove();
        }

        var objLinkTwit = $("#txtLinkTwitter");
        if (objInstitucion.LinkTwitter !== "") {

            $(objLinkTwit).click(function () {
                window.open("http://" + objInstitucion.LinkTwitter, '_blank');
                return false;
            });
        }
        else {
            $(objLinkTwit).remove();
        }

        var objLinkWeb = $("#txtWeb");
        if (objInstitucion.Web !== "") {

            $(objLinkWeb).click(function () {
                window.open("http://" + objInstitucion.Web, '_blank');
                return false;
            });
        }
        else {
            $(objLinkWeb).remove();
        }

        return false;

    });


    return false;

}

function CargarInstitucionesRelacionadas() {


    var Json = [{ ImagenPrograma: "ruta", Logo: "logo", Nombre: "Nombre", Titulo: "Titulo", DescripcionCorta: "Descripicon" }];

    HtmlLoadHandleBars($("#Related_Courses_List"), "../../templates/Courses.html", "Courses", Json);
    return false;

}

function CargarListaProgramas(intInstitucionId) {
    var lstProgramas = GetListaProgramas(intInstitucionId);
    if (lstProgramas !== undefined) {
        $(".txtTituloPrograma").click(function(){
            alert("wea");
        });
        $(".btnDetalles").click(function () {
            var id = $(this).data("id");
            Cookies.set('intProgramaId', id);
            //alert(id);
            window.open("../Programa/Programa.html");
        });
    }
}

function CargarListaUsuarios(intInstitucionId) {
    var lstUsuarios = GetListaUsuarios(intInstitucionId);
    if (lstUsuarios !== undefined) {
        $(".btnUsuario").click(function () {
            var id = $(this).data("id");
            Cookies.set('intUsuarioId', id);
            //alert(id);
            window.open("../Usuario/Usuario.html");
        });
    }
}
function GetListaProgramas(institucionId) {
    var lstProgramas;
    var then = function () {
        $('a[data-programaid]').click(function () {
            Cookies.set('programaid', $(this).data('programaid'));
            window.open('../Programa/Programa.html');
        });
    };
    AjaxPost("../wsPerfiles.asmx/GetListaProgramasInstitucion", "'intInstitucionId':" + institucionId, false, function (lstResult) {
        lstProgramas = lstResult;
        HtmlLoadHandleBars($("#Courses_List"), "../../templates/Courses.html", "Courses", lstResult, then);
    });
    return lstProgramas;
}

function GetListaUsuarios(institucionId) {
    var lstUsuarios;
    AjaxPost("../wsPerfiles.asmx/GetListaUsuariosInstitucion", "'intInstitucionId':" + institucionId, false, function (lstResult) {
        lstUsuarios = lstResult;
        HtmlLoadHandleBars($("#Contacts_List"), "../../templates/Institution_Contact.html", "Institution_Contact", lstUsuarios);
    });
    return lstUsuarios;
}