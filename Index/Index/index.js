$(document).ready(function () {
    loadPage_Annon();
    CargarCategoriasIndex();
    CargarUltimosProgramas();
});

Handlebars.registerHelper('ifEquals', function (arg1, arg2, options) {
    return (arg1 === arg2) ? options.fn(this) : options.inverse(this);
});
Handlebars.registerHelper('notEquals', function (arg1, arg2, options) {
    return (arg1 !== arg2) ? options.fn(this) : options.inverse(this);
});
Handlebars.registerHelper('greaterThan', function (arg1, arg2, options) {
    return (arg1 > arg2) ? options.fn(this) : options.inverse(this);
});

function CargarCategoriasIndex() {
    var lstCategorias;
    AjaxPost("../../wsObjetos.asmx/CargarCategoriasIndex", "", false, function (lstResult) {
        lstCategorias = lstResult;
        HtmlLoadHandleBars($("#Categorias"), "templates/Index.html", "Area_Category", lstCategorias);
    });
    return lstCategorias;
}

function CargarUltimosProgramas() {
    var lstProgramas;
    AjaxPost("../../wsObjetos.asmx/GetUltimosProgramas", "", false, function (lstResult) {
        lstProgramas = lstResult;
        HtmlLoadHandleBars($("#ultimos_cursos"), "templates/Index.html", "Area_Courses", lstProgramas);
    });
    return lstProgramas;
}