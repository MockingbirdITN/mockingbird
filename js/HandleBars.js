 //$(document).ready(function(){
 // var handlebarScript = document.createElement("script");
 // handlebarScript.type = "text/javascript"
 // handlebarScript.src = "../../../assets/js/libs/handlebars/handlebars-v4.0.11.js";

 // document.head.appendChild(handlebarScript);
 //});
 
 function HtmlLoad(Target, TemplatePath, TemplateID, JsonData) {
  var Elements = [];
  Elements['Element'] = [];
    for (var i = 0; i < JsonData.length; i++) {
    Elements['Element'].push(JsonData[i]);
    }
    Target.load(TemplatePath + " #" + TemplateID, function () {
      var Source = $('#' + TemplateID).html();
      var Template = Handlebars.compile(Source);
      var html = Template(Elements);
      Target.html(html);
    });
  };