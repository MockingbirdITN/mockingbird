var loadPage_Annon = function () {
    HtmlLoad($('#header'), '../../templates/Headers.html', 'header_Annon');
    HtmlLoad($('#footer'), '../../templates/Footers.html', 'footer_Annon');
};

var loadPage_User = function () {
    HtmlLoad($('#header'), '../../templates/Headers.html', 'header_User');
    HtmlLoad($('#footer'), '../../templates/Footers.html', 'footer_Annon');
};

function HtmlLoad(Target, TemplatePath, TemplateID) {
    Target.load(TemplatePath + " #" + TemplateID);
}