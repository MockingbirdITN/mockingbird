﻿/// <reference path="jquery-2.2.4.min.js" />
/// <reference path="loadMenu.js" />

function AjaxPost(strUrl, strParametros, bolAsync, fnSuccess) {

    $.ajax({
        type: "POST",
        url: strUrl,
        data: "{" + strParametros + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: bolAsync,
        success: function (result) {

            fnSuccess(result.d);

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

            if (errorThrown !== "") {
                alert("Error en Post: " + errorThrown);

            }
        }
    });
    return false;
}

function HtmlLoadHandleBars(Target, TemplatePath, TemplateID, JsonData,then) {
    var Elements = [];
    Elements['Element'] = [];
    for (var i = 0; i < JsonData.length; i++) {
        Elements['Element'].push(JsonData[i]);
    }
    Target.load(TemplatePath + " #" + TemplateID, function () {
        var Source = $('#' + TemplateID).html();
        var Template = Handlebars.compile(Source);
        var html = Template(Elements);
        Target.html(html);
        if (then !== undefined) {
            then();
        }
    });
    console.log(then);
    if (then !== null) {
        then;
    }
}

function CargarCombo(objCombo, lstItems) {
    HtmlLoadHandleBars(objCombo, '../../templates/ComboTemplate.html', 'ComboTemplate', lstItems);
    return false;
}

function GetInstitucionId() {
    //Obtener institucion de usuario logeado, crear cookie al log in con exito, objeto de usuario.
    return 1;
}

function GetUsuarioId() {
    //Obtener institucion de usuario logeado, crear cookie al log in con exito, objeto de usuario.
    return 1;
}


function ModalConfirmBootstrap(objConfiguracion) {

    $("body").append(
        '  <div class="modal fade" id="divModalConfirm" role="dialog">' +
        '    <div class="modal-dialog">' +
        '      <div class="modal-content">' +
        '        <div class="modal-header">' +
        '          <button type="button" class="close" data-dismiss="modal">&times;</button>' +
        '          <h4 class="modal-title">' + objConfiguracion.strTitulo + '</h4>' +
        '        </div>' +
        '        <div class="modal-body">' +
        '          ' + objConfiguracion.strBody +
        '        </div>' +
        '        <div class="modal-footer">' +
        '          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>' +
        '          <button type="button" class="btn btn-primary">' + objConfiguracion.strConfirmar + '</button>' +
        '        </div>' +
        '      </div>' +
        '    </div>' +
        '  </div>'
    );

    $("#divModalConfirm .btn-primary").click(function () {

        if (objConfiguracion.fnConfirmar !== undefined) {
            objConfiguracion.fnConfirmar(objConfiguracion.objParametros);
        }
        $('#divModalConfirm').modal('hide');
        return false;

    });

    $('#divModalConfirm').on('hidden.bs.modal', function () {
        if (objConfiguracion.fnCancelar !== undefined) {
            objConfiguracion.fnCancelar();
        }
        $(this).remove();
    });

    $('#divModalConfirm').modal('show');
}

function CheckSesion() {
    AjaxPost("../../wsInicioSesion.asmx/RevisarSesion", '', false, function (intTipoUsuario) {
        console.log(intTipoUsuario);
        if (intTipoUsuario === 0) {
            loadPage_Annon();
            return 0;
        }
        else if (intTipoUsuario === 1) {
            loadPage_User();
            return 1;
        }
    });
    return false;
}